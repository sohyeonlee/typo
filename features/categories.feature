Feature: Merge Articles
  As a blog administrator
  In order to manage blogging categories
  I want to be able to create new categories and edit existing ones

  Background:
    Given the admin user and non-admin user is set up
    And I am logged into the admin panel

  Scenario: Successfully access category page
  	When I follow "Categories"
	Then I should see "Name"
	Then I should see "Keywords"
	Then I should see "Permalink"
	
  Scenario: Successfully create new categories
    When I follow "Categories"
	And I fill in "category_name" with "new category"
	And I press "Save"
	Then I should see "new category"

  Scenario: Successfully edit existing categories
  	When I follow "Categories"
	And I follow "General"
	And I fill in "category_name" with "editted category"
	And I press "Save"
	Then I should see "editted category"
	Then I should not see "General"
	
	                                                               