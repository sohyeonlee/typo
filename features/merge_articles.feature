Feature: Merge Articles
  As a blog administrator
  In order to avoid repeating articles on the same topic
  I want to be able to merge relevant articles into one article

  Background:
    Given the admin user and non-admin user is set up
    And I have an article with id "3", title "first" and content "test1"
    And I have an article with id "4", title "second" and content "test2"

  Scenario: A non-admin cannot merge two articles
    Given I am logged into the non-admin panel
    When I follow "first"
    Then I should not see "Merge Articles"

  Scenario: When articles are merged
    Given I am logged into the admin panel
	When I follow "All Articles"
	When I follow "first"
	Then I should see "Merge Articles"
	When I fill in "Merge_id" with number 4
	And I press "Merge"
	Then I should see "test1"
	Then I should see "test2"